<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\PassiveSkills\PassiveSkillsService;

/**
 * @Route("/main/character")
 */
class PassiveSkillsController extends Controller
{
    /**
     * @Route("/skills", name="passive_skills")
     * @param PassiveSkillsService $passiveSkillsService
     * @return Response
     */
    public function index(PassiveSkillsService $passiveSkillsService)
    {
        $user = $this->getUser();

        $passiveSkills = $passiveSkillsService->showPassiveSkills();

        if ($passiveSkills !== null) {
            $passiveSkills['skillpoints'] = $user->getSkillpoints();
        }

        return $this->render('passive_skills/index.html.twig', [
            'passiveSkills' => $passiveSkills
        ]);
    }

    /**
     * @Route("/save-character-skills", name="save_character_skills")
     * @param Request $request
     * @param PassiveSkillsService $passiveSkillsService
     * @return JsonResponse|Response
     */
    public function saveCharacterSkills(Request $request, PassiveSkillsService $passiveSkillsService)
    {
        $characterSkills = (array)json_decode($request->request->get('skillData'));

        $response = new Response;

        if ($request->isXmlHttpRequest()) {
            $result = $passiveSkillsService->saveSkills($characterSkills);
            $response = new JsonResponse($result);
            return $response;
        }

        return $response;
    }
}
