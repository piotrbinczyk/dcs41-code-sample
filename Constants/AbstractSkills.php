<?php

namespace App\Constants;

abstract class AbstractSkills
{
    const WRONG_DATA_TYPE = [
        'msg' => 'Coś poszło nie tak.',
        'type' => 'error'
    ];

    const WRONG_SKILL_SUM = [
        'msg' => 'Nieprawidłowa suma punktów.',
        'type' => 'error'
    ];

    const WRONG_PARTICULAR_SKILL_VALUE = [
        'msg' => 'Nieprawidłowa wartość parametru.',
        'type' => 'error'
    ];

    const UNFILLED_CONDITION = [
        'msg' => 'Warunek niespełniony.',
        'type' => 'error'
    ];

    const TOO_FEW_DATA = [
        'msg' => 'Za mało danych.',
        'type' => 'error'
    ];

    const TOO_HIGH_SKILL_LEVEL = [
        'msg' => 'Za wysoki poziom umiejętności.',
        'type' => 'error'
    ];


    const NO_TRAITS_CHANGES = [
        'msg' => 'Brak zmian w cechach.',
        'type' => 'info'
    ];

    const SAVED_TRAITS_CHANGES = [
        'msg' => 'Zmiany zapisane pomyślnie.',
        'type' => 'success'
    ];


    const SUCCESS = 1;

    const REQUIRED_ARRAY_SIZE = 7;

    const REQUIRED_LEVEL_SIZE = 3;

    const REQUIRED_NUMBER_OF_SKILLS = 6;

    const MAXIMUM_SKILL_LEVEL = 8;


    const BACKSTAB = "Cios w plecy";

    const SNEAKING = "Skradanie";

    const MINING = "Otwieranie zamków";

    const BLACKSMITHING = "Rozbrajanie pułapek";
}
