<?php

namespace App\PassiveSkills;

use App\Services\UserData;
use App\Constants\AbstractSkills;

class PassiveSkillsService extends AbstractSkills
{
    private $userData;
    private $passiveSkillsQuery;
    private $passiveSkillsValidation;
    private $passiveSkillsCommand;

    public function __construct(
        UserData $userData,
        PassiveSkillsQueryRepository $passiveSkillsQuery,
        PassiveSkillsValidation $skillsValidation,
        PassiveSkillsCommandRepository $passiveSkillsCommand
    ) {
        $this->userData = $userData;
        $this->passiveSkillsQuery = $passiveSkillsQuery;
        $this->passiveSkillsValidation = $skillsValidation;
        $this->passiveSkillsCommand = $passiveSkillsCommand;
    }

    /**
     * Pobiera umiejętności pasywne do wyświetlenia
     * @return array|null
     */
    public function showPassiveSkills(): ?array
    {
        $skillsLevels = $this->passiveSkillsQuery->findCharacterPassiveSkillsLevels($this->userData->getUserId());

        if (!is_array($skillsLevels)) {
            return null;
        }

        if (count($skillsLevels) < AbstractSkills::REQUIRED_NUMBER_OF_SKILLS) {
            return null;
        }

        return [
            'athletics' => $skillsLevels[0]['level'],
            'sneaking' => $skillsLevels[1]['level'],
            'backstab' => $skillsLevels[2]['level'],
            'mechanics' => $skillsLevels[3]['level'],
            'mining' => $skillsLevels[4]['level'],
            'blacksmithing' => $skillsLevels[5]['level']
        ];
    }

    /**
     * Zapisuje zmiany w umiejętnościach pasywnych
     * @param $passiveSkills
     * @return array|null
     */
    public function saveSkills(array $passiveSkills): ?array
    {
        $checkIfSkillsValid = $this->passiveSkillsValidation->validate(
            $passiveSkills,
            $this->userData->getUserId(),
            $this->userData->getCharacterSkillpoints()
        );

        if (is_array($checkIfSkillsValid)) {
            return $checkIfSkillsValid;
        }

        $updateSkillsResult = $this->passiveSkillsCommand->updateSkillPointsAndSkills(
            $passiveSkills,
            $passiveSkills['skillpoints'],
            $this->userData->getUserId()
        );

        if ($updateSkillsResult == AbstractSkills::SUCCESS) {
            return AbstractSkills::SAVED_TRAITS_CHANGES;
        }

        return AbstractSkills::NO_TRAITS_CHANGES;
    }
}
