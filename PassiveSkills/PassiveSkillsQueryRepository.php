<?php

namespace App\PassiveSkills;

use Doctrine\ORM\EntityManagerInterface;

class PassiveSkillsQueryRepository
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function findCharacterPassiveSkillsLevels(int $accountId)
    {
        $query = $this->entityManager->createQuery(
            'SELECT a.level
                  FROM App\Entity\AccountHasPassiveSkills a 
                  JOIN App\Entity\PassiveSkills p 
                  WHERE a.accountAccount = :accountId 
                  AND p.passiveSkillsId = a.passiveSkillsPassiveSkillsId
                  ORDER BY p.passiveSkillsId ASC'
        )->setParameter('accountId', $accountId);

        return $query->getResult();
    }

    public function findCharacterPassiveSkillsLevelsAndNames(int $accountId)
    {
        $query = $this->entityManager->createQuery(
            'SELECT a.level, p.name
                  FROM App\Entity\AccountHasPassiveSkills a 
                  JOIN App\Entity\PassiveSkills p 
                  WHERE a.accountAccount = :accountId 
                  AND p.passiveSkillsId = a.passiveSkillsPassiveSkillsId
                  ORDER BY p.passiveSkillsId ASC'
        )->setParameter('accountId', $accountId);

        return $query->getResult();
    }
}
