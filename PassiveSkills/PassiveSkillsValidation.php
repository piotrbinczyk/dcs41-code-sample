<?php

namespace App\PassiveSkills;

use App\Constants\AbstractSkills;

class PassiveSkillsValidation extends AbstractSkills
{
    private $passiveSkillsQuery;
    private $currentQuantitySkillPoints;
    private $newQuantitySkillPoints;

    public function __construct(PassiveSkillsQueryRepository $passiveSkillsQuery)
    {
        $this->passiveSkillsQuery = $passiveSkillsQuery;
    }

    /**
     * Główna metoda walidująca umiejętności postaci
     * @param $characterSkills
     * @param int $characterId
     * @param int $skillPoints
     * @return array|null
     */
    public function validate($characterSkills, int $characterId, int $skillPoints): ?array
    {
        $errorList = [
            'dataTypeCorrectness' => $this->checkDataTypeCorrectness($characterSkills),
            'arrayLength' => $this->checkArrayLength($characterSkills),
            'eachSkillSize' => $this->checkEachSkillSize($characterSkills, $characterId, $skillPoints),
            'skillpointsSum' => $this->checkSkillPointsSum($characterSkills, $characterId, $skillPoints),
            'ifMeetsRequirements' => $this->checkIfMeetsRequirements($characterSkills, $characterId)
        ];

        foreach ($errorList as $errors) {
            if (is_array($errors)) {
                return $errors;
            }
        }

        return null;
    }

    /**
     * Sprawdza czy przesłane dane to tablica
     * @param $characterSkills
     * @return array|null
     */
    private function checkDataTypeCorrectness($characterSkills): ?array
    {
        if (!is_array($characterSkills)) {
            return AbstractSkills::WRONG_DATA_TYPE;
        }

        return null;
    }

    /**
     * Sprawdza czy tablica jest wymaganej długości
     * @param array $characterSkills
     * @return array|null
     */
    private function checkArrayLength(array $characterSkills): ?array
    {
        if (count($characterSkills) !== AbstractSkills::REQUIRED_ARRAY_SIZE) {
            return AbstractSkills::TOO_FEW_DATA;
        }

        return null;
    }

    /**
     * Sprawdza czy przesłane wartości nie są mniejsze od tych w bazie danych
     * @param array $characterSkills
     * @param int $characterId
     * @param int $skillPoints
     * @return array|null
     */
    private function checkEachSkillSize(array $characterSkills, int $characterId, int $skillPoints): ?array
    {
        $currentUserSkills = $this->passiveSkillsQuery->findCharacterPassiveSkillsLevelsAndNames($characterId);

        for ($i =0; $i <= 5; $i++) {
            if ($characterSkills[$currentUserSkills[$i]['name']] < $currentUserSkills[$i]['level']) {
                return AbstractSkills::WRONG_PARTICULAR_SKILL_VALUE;
            }
        }

        if ($characterSkills['skillpoints'] > $skillPoints) {
            return AbstractSkills::WRONG_PARTICULAR_SKILL_VALUE;
        }

        return null;
    }

    /**
     * Sprawdza czy całkowita suma przesłanych wartości jest równa tym w bazie danych
     * @param array $characterSkills
     * @param int $characterId
     * @param int $skillPoints
     * @return array|null
     */
    private function checkSkillPointsSum(array $characterSkills, int $characterId, int $skillPoints): ?array
    {
        $userSkillsLevels = $this->passiveSkillsQuery->findCharacterPassiveSkillsLevels($characterId);

        foreach ($userSkillsLevels as $current) {
            $this->currentQuantitySkillPoints += $current['level'];
        }

        $this->currentQuantitySkillPoints += $skillPoints;

        foreach ($characterSkills as $new) {
            $this->newQuantitySkillPoints += $new;
        }

        if ($this->newQuantitySkillPoints !== $this->currentQuantitySkillPoints) {
            return AbstractSkills::WRONG_SKILL_SUM;
        }

        return null;
    }

    /**
     * Sprawdza, która umiejętność ma być zaktualizowana
     * @param array $characterSkills
     * @param int $characterId
     * @return array|null
     */
    private function checkIfMeetsRequirements(array $characterSkills, int $characterId): ?array
    {
        $result = null;

        $currentUserSkills = $this->passiveSkillsQuery->findCharacterPassiveSkillsLevels($characterId);

        foreach ($characterSkills as $key => $new) {
            switch ($key) {
                case AbstractSkills::SNEAKING:
                    $result = $this->checkEachSkillRequirement($new, $currentUserSkills[0]['level']);
                    break;
                case AbstractSkills::BACKSTAB:
                    $result = $this->checkEachSkillRequirement($new, $currentUserSkills[1]['level']);
                    break;
                case AbstractSkills::MINING:
                    $result = $this->checkEachSkillRequirement($new, $currentUserSkills[3]['level']);
                    break;
                case AbstractSkills::BLACKSMITHING:
                    $result = $this->checkEachSkillRequirement($new, $currentUserSkills[4]['level']);
                    break;
            }

            if (!is_null($result)) {
                return $result;
            }
        }

        return $this->checkMaximumSkillValue($characterSkills);
    }

    /**
     * Sprawdza czy wymagania do akutalizacji umiejętności zostały spełnione
     * @param $skill
     * @param $currentSkillLevel
     * @return array|null
     */
    public function checkEachSkillRequirement(int $skill, int $currentSkillLevel): ?array
    {
        if ($skill > 0 && $currentSkillLevel < AbstractSkills::REQUIRED_LEVEL_SIZE) {
            return AbstractSkills::UNFILLED_CONDITION;
        }

        return null;
    }

    /**
     * Sprawdza czy umiejętności nie przekroczyły maksymalnego poziomu
     * @param array $characterSkills
     * @return array|null
     */
    private function checkMaximumSkillValue(array $characterSkills): ?array
    {
        unset($characterSkills['skillpoints']);

        foreach ($characterSkills as $skill) {
            if ($skill > AbstractSkills::MAXIMUM_SKILL_LEVEL) {
                return AbstractSkills::TOO_HIGH_SKILL_LEVEL;
            }
        }

        return null;
    }
}
