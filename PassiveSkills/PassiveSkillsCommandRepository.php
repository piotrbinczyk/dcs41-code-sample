<?php

namespace App\PassiveSkills;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class PassiveSkillsCommandRepository
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function updateSkillPoints(int $skillPoints, int $characterId)
    {
        $query = $this->entityManager->createQuery(
            'UPDATE App\Entity\Account a SET 
              a.skillPoints = :skillPoints
              WHERE a.accountId = :characterId'
        )->setParameters(array(
            'skillPoints' => $skillPoints,
            'characterId' => $characterId
        ));

        return $query->execute();
    }

    public function updateSkillPointsAndSkills(array $passiveSkills, int $skillPoints, int $characterId)
    {
        $this->entityManager->beginTransaction();
        try {
            $query = $this->entityManager->createQuery(
                'UPDATE App\Entity\AccountHasPassiveSkills a SET
                a.level = (CASE 
                WHEN a.passiveSkillsPassiveSkillsId = 1 THEN :athletics 
                WHEN a.passiveSkillsPassiveSkillsId = 2 THEN :sneaking
                WHEN a.passiveSkillsPassiveSkillsId = 3 THEN :backstab
                WHEN a.passiveSkillsPassiveSkillsId = 4 THEN :mechanics
                WHEN a.passiveSkillsPassiveSkillsId = 5 THEN :mining
                WHEN a.passiveSkillsPassiveSkillsId = 6 THEN :blacksmithing ELSE :blacksmithing
                END)
            WHERE a.passiveSkillsPassiveSkillsId IN(1, 2, 3, 4, 5, 6) AND a.accountAccount = :character_id'
            )->setParameters(array(
                'athletics' => $passiveSkills['Atletyka'],
                'sneaking' => $passiveSkills['Skradanie'],
                'backstab' => $passiveSkills['Cios w plecy'],
                'mechanics' => $passiveSkills['Mechanika'],
                'mining' => $passiveSkills['Górnictwo'],
                'blacksmithing' => $passiveSkills['Kowalstwo'],
                'character_id' => $characterId,
            ));

            $query->execute();

            $query = $this->entityManager->createQuery(
                'UPDATE App\Entity\Account a SET 
              a.skillPoints = :skillPoints
              WHERE a.accountId = :characterId'
            )->setParameters(array(
                'skillPoints' => $skillPoints,
                'characterId' => $characterId
            ));

            $query->execute();

            $this->entityManager->getConnection()->commit();
        } catch (Exception $e) {
            $this->entityManager->getConnection()->rollBack();
        }

        return 1;
    }
}
