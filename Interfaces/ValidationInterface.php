<?php

namespace App\Interfaces;

interface ValidationInterface
{
    public function validate($data): ?array;
}
